# Plan
## Goal
Per the test:
> Using the provided API, create a program that retrieves a datasetId, retrieves all vehicles and dealers for that dataset, and successfully posts to the answer endpoint. Each vehicle and dealer should be requested only once. You will receive a response structure when you post to the answer endpoint that describes status and total ellapsed time; your program should output this response.

> The server has a built in delay that varies between 1 and 4 seconds for each request for a vehicle or for a dealer. Focus on optimizing for low total elapsed time between retrieving the datasetid and posting the answer. A successful submission will complete in significantly less than 30 seconds.
## Steps
1. get datasetId
1. get vehicle list for datasetId
    1. Iterate through to retrieve all info on each car
        - Stash dealer IDs for later
        - Note: Watch for dupes on vehicles & dealers
1. Gather info on dealers
1. Assemble final result
1. Post result to /answer
1. Output response from /answer
## Notes
- Deduping is a must.
- Probably manage data in JSON to KISS.
- Function for hitting the API to keep it DRY
- Reusable iterator for "get details of this array"?
    - Watch out for unnecessary complexity
- Python: I'm thinking `requests`
- Document and comment
- It's too slow.  Going to need to multithread
