#!/usr/bin/env python3
import requests
import threading
import sys


class AutoInterview:
    base_url = 'http://vautointerview.azurewebsites.net/api/'
    headers = {'Content-Type': 'application/json'}

    def __init__(self):
        self.datasetId = ''
        self.answer = {"dealers": []}
        self.vehicle_info = {}    # Will hold detailed vehicle info for later
        self.dealer_vehicle = {}  # Will be linking dict between dealer IDs and list of vehicle IDs

    def api_get(self, endpoint):
        try:
            full_url = self.base_url + endpoint
            response = requests.get(full_url, headers=self.headers)
            response.raise_for_status()
            return response.json()
        except requests.RequestException as e:
            print(e)
            sys.exit(1)

    def api_post(self, endpoint, data):
        try:
            full_url = self.base_url + endpoint
            response = requests.post(full_url, headers=self.headers, json=data)
            return response.json()
        except requests.RequestException as e:
            print(e)
            sys.exit(1)

    def add_vehicle_info(self, vehicle_id):
        self.vehicle_info[vehicle_id] = self.api_get(self.datasetId + '/vehicles/' + str(vehicle_id))
        thread_data = threading.local()
        thread_data.current_dealer_id = self.vehicle_info[vehicle_id]['dealerId']
        if thread_data.current_dealer_id not in self.dealer_vehicle:
            # We've never seen this dealer before!  Add what info we have to the final answer
            self.dealer_vehicle[thread_data.current_dealer_id] = []
            self.answer['dealers'].append({"dealerId": thread_data.current_dealer_id, "vehicles": []})
            # and add the vehicle ID to the linking dict
        self.dealer_vehicle[thread_data.current_dealer_id].append(vehicle_id)
        self.vehicle_info[vehicle_id].pop('dealerId')  # Remove the dealerId as it's not wanted in final results

    def add_dealer_info(self, dealer_index, dealer_id):
        for vehicle_id in self.dealer_vehicle[dealer_id]:
            self.answer['dealers'][dealer_index]['vehicles'].append(self.vehicle_info[vehicle_id])
        dealer_info = self.api_get(self.datasetId + '/dealers/' + str(dealer_id))
        self.answer['dealers'][dealer_index]['name'] = dealer_info['name']

    def run(self):
        # Step 1: get datasetId
        self.datasetId = self.api_get('datasetId')['datasetId']

        # Step 2: get vehicle list
        vehicle_json = self.api_get(self.datasetId + '/vehicles')

        # Step 3: iterate through list and launch a thread to retrieve each vehicle's info
        for vehicle_id in vehicle_json['vehicleIds']:
            if vehicle_id in self.vehicle_info:
                continue  # Make sure we don't retrieve a vehicle more than once
            t1 = threading.Thread(target=self.add_vehicle_info, args=[vehicle_id])  # Launch thread
            t1.start()  # Tell thread to begin work

        # We don't want to move on until all threads have finished
        while threading.active_count() > 1:
            pass

        # Step 4: Gather info on dealers and assemble final result
        seen_dealers = []    # Simple dedupe list
        for index, dealer in enumerate(self.answer['dealers']):
            dealer_id = dealer['dealerId']
            if dealer_id in seen_dealers:
                continue
            seen_dealers.append(dealer_id)
            t1 = threading.Thread(target=self.add_dealer_info, args=[index, dealer_id])
            t1.start()
            while threading.active_count() > 1:
                pass

        # Step 5: Post result to /answer
        final_response = self.api_post(self.datasetId + '/answer', self.answer)
        # Step 6: Outpost response from /answer
        print(final_response)
        return


AutoInterview().run()
